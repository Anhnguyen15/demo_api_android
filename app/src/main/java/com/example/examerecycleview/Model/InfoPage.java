package com.example.examerecycleview.Model;

import java.util.ArrayList;
import java.util.List;

public class InfoPage {
    private int page;
    private int per_page;
    private int total_pages;
    private ArrayList<Student> data;

    public InfoPage(int page, int per_page, int total_pages, ArrayList<Student> data) {
        this.page = page;
        this.per_page = per_page;
        this.total_pages = total_pages;
        this.data = data;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public ArrayList<Student> getData() {
        return data;
    }

    public void setData(ArrayList<Student> data) {
        this.data = data;
    }
}

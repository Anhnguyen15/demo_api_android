package com.example.examerecycleview.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.examerecycleview.Adapter.StudentAdapter;
import com.example.examerecycleview.Model.InfoPage;
import com.example.examerecycleview.Model.Student;
import com.example.examerecycleview.R;
import com.example.examerecycleview.Service.ApiService;
import com.example.examerecycleview.SpacesItemDecoration;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    StudentAdapter studentAdapter;
    ArrayList<Student> mStudents;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindView();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_modify_3:
                getAllUsers();
                return true;
            default:break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getAllUsers() {
        ApiService.apiService.getInfoPage(2).enqueue(new Callback<InfoPage>() {
            @Override
            public void onResponse(Call<InfoPage> call, Response<InfoPage> response) {
                getSrudent(response.body().getData());
            }

            @Override
            public void onFailure(Call<InfoPage> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Error request", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSrudent(ArrayList<Student> listStudents) {
        mStudents = new ArrayList<>();
        mStudents = listStudents;
        studentAdapter = new StudentAdapter(mStudents, MainActivity.this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setAdapter(studentAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new SpacesItemDecoration(50));
    }
    private void bindView() {
        recyclerView = findViewById(R.id.studentsList);
    }
}
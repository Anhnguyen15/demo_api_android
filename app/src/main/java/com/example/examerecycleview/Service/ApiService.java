package com.example.examerecycleview.Service;

import com.example.examerecycleview.Model.InfoPage;
import com.example.examerecycleview.Model.Student;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    public static final String BASE_URL = "https://reqres.in/api/";
    ApiService apiService = new Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService.class);

    @GET("users/")
    Call<InfoPage> getInfoPage(@Query("page") int page);

}

package com.example.examerecycleview.Adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.examerecycleview.R;
import com.example.examerecycleview.Model.Student;
import com.squareup.picasso.Picasso;

import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> {
    private List<Student> mStudents;
    private Context mContext;

    public StudentAdapter(List<Student> mStudents, Context mContext) {
        this.mStudents = mStudents;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context parentContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parentContext);
        View studentViewItem = inflater.inflate(R.layout.student_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(studentViewItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Student student = mStudents.get(position);
        holder.studentName.setText(student.getFirst_name());
        holder.id.setText(String.valueOf(student.getId()));
        holder.email.setText(student.getEmail());
        Picasso.get().load(student.getAvatar()).into(holder.avatar);
    }

    @Override
    public int getItemCount() {
        return mStudents.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder {
        private View itemView;
        public TextView studentName;
        public TextView id;
        public TextView email;
        public ImageView avatar;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.studentName = itemView.findViewById(R.id.studentName_txt);
            this.id = itemView.findViewById(R.id.id_txt);
            this.email = itemView.findViewById(R.id.email_txt);
            this.avatar = itemView.findViewById(R.id.avatarImg);
        }
    }
}
